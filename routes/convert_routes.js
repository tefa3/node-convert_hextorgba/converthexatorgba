const express=require('express')

const app = express()

app.use(express.json())

const convertController = require("../controller/convert_controller")

app.get("/:hexa", convertController.convertColor)

module.exports=app