# Convert HEX to RGBA

Input your hex and automaticaly change to RGBA 


## Documentation

![Hit](https://raw.githubusercontent.com/setyawannnIMG/hextorgbatefa6/main/hextorgbapng.png)

## Author

- Prayoga Adi Setyawan [@setyawannn](https://www.gitlab.com/setyawannn)
- Reyhan Marsalino Diansa [@ReyhanDiansa](https://gitlab.com/ReyhanDiansa)
- Zhidan Marties Alfareza [@lakyulakyu](https://gitlab.com/lakyulakyu)
- Zaskia Rizky Raichand [@zaskiakiaa802](https://gitlab.com/zaskiakiaa802)


## Tools 
- Node Js
- VS Code
- Postman