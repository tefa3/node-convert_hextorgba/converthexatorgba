const express = require(`express`)

const app = express()
const port=8080
const cors =require(`cors`)
app.use(cors())

const convertRoute= require(`./routes/convert_routes`)
app.use(`/api`,convertRoute)

app.listen(port,()=>{
    console.log(`server of convert Hexa To RGBA runs on port ${port}`)
})